/// <reference types="cypress" />


// test case dengan nama empty test
describe('Empty test', () => {
    

    // test dengan syntax it 
    it('tes one', () => {

        // visit website
        cy.visit('https://openai.com/')

        // mengecek apakah div dengan class container ada
        cy.get('div.container').should('exist')

        // menegecek apakah div mempunyai class container
        cy.get('div').should('have.class', 'container')

        // cek apakah div dengan id no root tidak ada
        cy.get('div#noroot').should('not.exist')

        // test dengan mengambil class container lalu div div dan a
        cy.get('.container > div > div > a')

        // mengambil id dari openai-horizontal lalu di click
        cy.get('#openai-horizontal').click()
    })

})